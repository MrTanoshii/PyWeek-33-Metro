<div align="center">

  <h3>Theme is "Evil Twin"</h3>

[![PyWeek 33](https://img.shields.io/badge/PyWeek-33-blue)](https://pyweek.org/33/)
[![PyWeek 33 Rules - Revision 2020-02-06](https://img.shields.io/badge/Rules-2020--02--06-blue)](https://pyweek.readthedocs.io/en/latest/rules.html)
[![Team Metroids](https://img.shields.io/badge/Team-Metroids-brightgreen)](https://pyweek.org/e/meme_py123/)
[![Game Name - Metro](https://img.shields.io/badge/Game-Metro-brightgreen)](https://pyweek.org/e/meme_py123/)
[![Game Theme - My evil twin](https://img.shields.io/badge/Game%20Theme-My%20evil%20twin-blue)](https://pyweek.org/p/37/)

</div>
<div align="center">

[![Python v3.10](https://img.shields.io/badge/Python-v3.10-blue)](https://docs.python.org/3.10/)
[![Arcade v2.6.11](https://img.shields.io/badge/Arcade-v2.6.11-blue)](https://api.arcade.academy/en/2.6.11/)

</div>
<div align="center">

[![Python Check](https://github.com/MrTanoshii/PyWeek-33-Metro/actions/workflows/python_check.yml/badge.svg)](https://github.com/MrTanoshii/PyWeek-33-Metro/actions/workflows/python_check.yml)

</div>

# PyWeek 33 | Team Metroids | The Epic of Goat

Featuring

- Jeb | https://github.com/JesperKauppinen
- MrTanoshii | https://github.com/MrTanoshii
- Memehunter | https://github.com/mohith01
- Cat | https://github.com/Catto-YFCN
- Krzysztof | https://github.com/IrrationalBoolean
- ATC_Tower | https://github.com/BriscoRP
